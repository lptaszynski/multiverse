# Multiverse

Riak core based framework for distributing gaming worlds.
It's my first riak core application and just a proof of concept. 

The main concept is to split game world into basic units of computation called "chunks" and specify links between
them for synchronized data exchange.
In provided example of Conway's Game of Life each chunk depends on edges and corners of 8 neighbours. 

# Build
Application depends on Erlang 17R.

    git clone https://github.com/lptaszynski/multiverse.git
    cd multiverse
    make devrel
    
here you will probably get some error saying that some types are deprecated.
To fix it you can use script I've found in FlavioDB code base: 
[fix_deps_warnings_as_errors.sh](https://github.com/marianoguerra/flaviodb/blob/master/util/fix_deps_warnings_as_errors.sh)

    wget https://raw.githubusercontent.com/marianoguerra/flaviodb/master/util/fix_deps_warnings_as_errors.sh && chmod u+x fix_deps_warnings_as_errors.sh
    ./fix_deps_warnings_as_errors.sh
    make devrel

# Try it

When it's done there should appear new `dev` directory. It contains three erlang releases, ready to become your cluster.
But for now let's stay with one:

    ./dev/dev1/bin/multiverse console
    
to start game of life type:

    game_of_life:demo().
    
Note names of hosting node above each chunk.
Let's start another node in second terminal:

    ./dev/dev2/bin/multiverse console
    
To distribute your board, you need to join nodes, check plan and commit vnodes migration.

    ./dev/dev2/bin/multiverse-admin cluster join multiverse1@127.0.0.1
    ./dev/dev2/bin/multiverse-admin cluster plan
    ./dev/dev2/bin/multiverse-admin cluster commit
    
Now you should see chunks migrating.
It is possible for node to safely leave riak cluster without violating game integrity.

    ./dev/dev2/bin/multiverse-admin cluster leave multiverse2@127.0.0.1
    ./dev/dev2/bin/multiverse-admin cluster plan
    ./dev/dev2/bin/multiverse-admin cluster commit

Try to create new world with:

    game_of_life:register_world(world_name, [{tick_delay, 200}], [{board_range, {{0,1},{0,1}}}]).
    game_of_life:start_chunks(world_name).
    game_of_life:print_to_console(world_name).

This creates new 2x2 game of life instance with default setup and overriden tick delay to 200ms.
Some of configuration can be overriden in live thanks to `riak_core_metadata`, try:

    multiverse_world_config:set_config(world_name, tick_delay, 10).

Enjoy!

    multiverse1@127.0.0.1      multiverse2@127.0.0.1      multiverse2@127.0.0.1      multiverse2@127.0.0.1
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][*]   [*][*][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][*][*]   [*][*][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][*][*]   [ ][ ][*][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][*][ ]   [ ][ ][*][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][*]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][*][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][*][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][*]   [ ][ ][*][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    multiverse2@127.0.0.1      multiverse2@127.0.0.1      multiverse2@127.0.0.1      multiverse1@127.0.0.1
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][*][*]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][*][*]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][*][*][*]   [*][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][*][*][*]   [*][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][*][*][ ]   [*][*][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][*][*][ ]   [*][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][*]   [*][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][*]   [*][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    multiverse2@127.0.0.1      multiverse2@127.0.0.1      multiverse2@127.0.0.1      multiverse1@127.0.0.1
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][*][*]   [*][*][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][*][*]   [*][ ][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][*][ ]   [ ][ ][*][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][*][ ]   [ ][*][*][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][*][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][*][ ]   [*][*][*][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][*]   [ ][ ][*][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][*]   [*][*][ ][ ][ ][ ][ ][ ]
    [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]   [ ][ ][ ][ ][ ][ ][ ][ ]

