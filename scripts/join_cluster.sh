#!/usr/bin/env sh

for d in dev/dev[!1]; do $d/bin/multiverse-admin cluster join multiverse1@127.0.0.1; done
dev/dev1/bin/multiverse-admin cluster plan
dev/dev1/bin/multiverse-admin cluster commit