-module(game_of_life_proxy).
-behaviour(multiverse_client_proxy).
%% API
-export([multicast/5]).

%% TODO implement proper networking
multicast(_World, _ChunkId, _Tick, [], _UpdateData) -> ok;
multicast(World, ChunkId, Tick, [HClient | TailClients], UpdateData) ->
  HClient ! {update, World, ChunkId, Tick, UpdateData},
  multicast(World, ChunkId, Tick, TailClients, UpdateData).

