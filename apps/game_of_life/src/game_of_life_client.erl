-module(game_of_life_client).

-behaviour(gen_server).

%% API
-export([start_link/0, start_link/1, subscribe/2, unsubscribe/2]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).
-record(state, {world, heart_tref, subscriptions = #{}, awaiting = #{}, tick_acc = #{}}).

%% API

subscribe(Pid, Id) ->
  gen_server:call(Pid, {subscribe, Id}).

unsubscribe(Pid, Id) ->
  gen_server:call(Pid, {subscribe, Id}).

start_link() -> start_link(game_of_life).
start_link(World) -> gen_server:start_link(?MODULE, [World], []).

%% gen_server callbacks

init([World]) ->
  {ok, TRef} = timer:send_interval(5 * 1000, send_heart),
  {ok, #state{world = World, heart_tref = TRef}}.

handle_call({subscribe, Id}, _From, State = #state{world = World, awaiting = Awaiting0}) ->
  {ok, Ref} = multiverse_client_proxy:subscribe(World, Id, self()),
  {reply, ok, State#state{awaiting = maps:put(Ref, Id, Awaiting0)}};

handle_call({subscribe, Id}, _From, State = #state{world = World, awaiting = Awaiting0}) ->
  {ok, Ref} = multiverse_client_proxy:unsubscribe(World, Id, self()),
  {reply, ok, State#state{awaiting = maps:put(Ref, Id, Awaiting0)}};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(send_heart, State = #state{world = World, subscriptions = Subscriptions}) ->
  [multiverse_client_proxy:heart(World, Id, self()) || Id <- maps:keys(Subscriptions)],
  {noreply, State};

handle_info({update, World, Id, Tick, {Node, _Update}}, State = #state{world = World}) ->
  io:format("Client ~p received tick ~p update for chunk ~p ~p from: ~p ~n", [self(), Tick, World, Id, Node]),
  {noreply, State};

handle_info(Info, State) ->
  io:format("CLIENT INFO: ~n~p~n", [Info]),
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% Internal functions
