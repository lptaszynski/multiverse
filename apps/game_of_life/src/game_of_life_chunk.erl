-module(game_of_life_chunk).

-behaviour(multiverse_chunk).

%% multiverse_chunk callbacks
-export([
  prepare_resources/1,
  init_persisted/2,
  init_migrated/2,
  handle_event/3,
  handle_get_request/3,
  client_subscribe/2,
  client_unsubscribe/3,
  update_clients/2,
  extract_sync_data/2,
  receive_sync_data/3,
  sync_data_links/3,
  tick/2,
  persist_state/1,
  migrate_state/1,
  handle_info/2,
  terminate/2]).

%% internal functions but i get dialyzer warnings if not exported: Function * will never be called
-export([
  fragments_to_deliver/4,
  fragments_to_expect/4,
  maybe_wrap_chunk/4,
  maybe_wrap/3,
  filter_exceeds/1]).


-include("game_of_life.hrl").

-record(state, {world, id, board, x_range, y_range, static_deliveries, static_expectations}).

%% multiverse_chunk callbacks

prepare_resources({World, Id}) -> % nothing to prepare here
  {ok, #state{world = World, id = Id}}.

init_persisted(Data, State = #state{world = World, id = Id = {_X, _Y}}) ->
  {XRange, YRange} = multiverse_world_config:get_metadata(World, board_range), % get chunks range
  Wrap = multiverse_world_config:get_metadata(World, wrap), % get chunks range
  Board = game_of_life_board:from_data(Data),
  DF = fragments_to_deliver(Id, XRange, YRange, Wrap),
  EF = fragments_to_expect(Id, XRange, YRange, Wrap),
  {ok, State#state{board = Board, static_deliveries = DF, static_expectations = EF, x_range = XRange, y_range = YRange}}.

init_migrated(MigrateState, _ChunkState) -> % state from prepare is not important in this case
  {ok, MigrateState}.

handle_event(_Event, _Client, State) -> %% TODO some basic client actions
  {reply, not_implemented, State}.

handle_get_request(_GetRequest, _Client, State) -> %% TODO geting local board should be here
  {ok, not_implemented, State}.

client_subscribe(Client, ChunkState = #state{world = World, id = Id, board = Board}) ->
  lager:debug("~p subscribes to ~p", [Client, {World, Id}]),
  SyncData = game_of_life_board:get_sliced_state({0, 0}, Board),
  {allow, SyncData, ChunkState}.

client_unsubscribe(Client, Reason, ChunkState = #state{world = World, id = Id}) ->
  lager:debug("~p unsubscribes ~p, reason ~p", [{World, Id}, Client, Reason]),
  {ok, ChunkState}.

update_clients({multicast, _Clients} = Method, ChunkState = #state{world = World, id = Id, board = Board}) ->
  lager:debug("~p updating clients ~p ", [{World, Id}, Method]),
  Response = game_of_life_board:get_sliced_state({0, 0}, Board),
  {ok, {node(), Response}, ChunkState}.

tick(_N, State = #state{board = Board}) ->
  NewBoard = game_of_life_board:transform(fun transform_cell/3, Board),
  {ok, State#state{board = NewBoard}}.

extract_sync_data({_DestinationId, DataId}, State = #state{board = Board}) ->
  Response = game_of_life_board:get_sliced_state(DataId, Board),
  {ok, Response, State}.

receive_sync_data({_SourceId, DataId}, Data, State = #state{board = Board}) ->
  NewBoard = game_of_life_board:store_neighbour_sliced_state(DataId, Data, Board),
  {ok, State#state{board = NewBoard}}.

sync_data_links(_N, _Id, State = #state{static_deliveries = StaticDeliveries, static_expectations = StaticExpectations}) ->
  {ok, {deliver, StaticDeliveries}, {expect, StaticExpectations}, State}.

persist_state(#state{board = Board}) ->
  Data = game_of_life_board:to_data(Board),
  {ok, Data}.

migrate_state(State) ->
  {ok, State}.

handle_info(Message, ChunkState = #state{world = World, id = Id}) ->
  lager:debug("~p handle info from ~p ", [{World, Id}, Message]),
  {ok, ChunkState}.

terminate(_Reason, _State) -> ok.

%% Internal functions

transform_cell(CellValue, {X, Y}, Array) ->
  NeighbourCells = [game_of_life_board:get(X + Xd, Y + Yd, Array) || Yd <- [1, 0, -1], Xd <- [-1, 0, 1], (Yd /= 0) orelse (Xd /= 0)],
  AliveCells = count_cells(?ALIVE, NeighbourCells),
  case CellValue of
    ?DEAD ->
      case AliveCells of
        3 -> ?ALIVE;
        _ -> ?DEAD
      end;
    ?ALIVE ->
      case AliveCells of
        2 -> ?ALIVE;
        3 -> ?ALIVE;
        _ -> ?DEAD
      end
  end.

count_cells(CellValue, NeighbourCells) ->
  lists:foldl(
    fun
      (Value, Acc) when CellValue == Value -> Acc + 1;
      (_, Acc) -> Acc
    end,
    0,
    NeighbourCells).


fragments_to_deliver({X, Y}, XRange, YRange, Wrap) ->
  filter_exceeds([{_DestChunkId = maybe_wrap_chunk({X + Xn, Y + Yn}, XRange, YRange, Wrap), _SendFragmentId = {Xn, Yn}} ||
    Yn <- [1, 0, -1], Xn <- [-1, 0, 1], (Yn /= 0) orelse (Xn /= 0)]).

fragments_to_expect({X, Y}, XRange, YRange, Wrap) ->
  filter_exceeds([{_SourceChunkId = maybe_wrap_chunk({X + Xn, Y + Yn}, XRange, YRange, Wrap), _ExpectFragmentId = {-Xn, -Yn}} ||
    Yn <- [1, 0, -1], Xn <- [-1, 0, 1], (Yn /= 0) orelse (Xn /= 0)]).

maybe_wrap_chunk({X, Y}, XRange, YRange, Wrap) ->
  {maybe_wrap(X, XRange, Wrap), maybe_wrap(Y, YRange, Wrap)}.

maybe_wrap(Coord, {CoordMin, CoordMax}, _Wrap) when Coord >= CoordMin, Coord =< CoordMax -> Coord;
maybe_wrap(_Coord, {_CoordMin, _CoordMax}, false) -> exceed;
maybe_wrap(Coord, {CoordMin, CoordMax}, true) when Coord == (CoordMin - 1) -> CoordMax;
maybe_wrap(Coord, {CoordMin, CoordMax}, true) when Coord == (CoordMax + 1) -> CoordMin.

filter_exceeds(List) ->
  lists:filter(
    fun({{exceed, _}, _}) -> false;
      ({{_, exceed}, _}) -> false;
      ({_, _}) -> true
    end,
    List).

%% fragment id explanation.
%% four single cell corners; two N-size edges; two M-size edges and {0,0} is NxM sized full array
%%
%%  1{-1, 1}--N{0, 1}--1{1, 1}
%%      |                 |
%%  M{-1, 0} NxM{0,0}  M{1, 0}
%%      |                 |
%%  1{-1,-1}--N{0,-1}--1{1,-1}



