-module(game_of_life_board).

%% API
-export([from_list/3, from_data/1, to_data/1, new/2, get/3, transform/2, transform/3, get_sliced_state/2, store_neighbour_sliced_state/3]).

-record(board, {n, m, arrays}).

new(N, M) -> #board{n = N, m = M, arrays = #{}}.

from_list(N, M, List) ->
  #board{n = N, m = M, arrays = #{{0, 0} => array:from_list(List)}}.

from_data(Data) ->
  {N, M} = proplists:get_value(size, Data),
  List = proplists:get_value(array, Data),
  from_list(N, M, List).

to_data(#board{n = N, m = M, arrays = Arrays}) ->
  LocalArray = maps:get({0, 0}, Arrays),
  [{size, {N, M}}, {array, array:to_list(LocalArray)}].

get(N, M, #board{n = NSize, m = MSize, arrays = Arrays}) ->
  ArrayId = translate_to_array_id(N, NSize, M, MSize),
  Array = maps:get(ArrayId, Arrays, undefined),
  case Array of
    undefined ->
      undefined;
    _ ->
      Index = coords_to_index(ArrayId, N, NSize, M, MSize),
      array:get(Index, Array)
  end.


get_sliced_state(Id, Board = #board{n = NSize, m = MSize, arrays = Arrays}) ->
  LocalArray = maps:get({0, 0}, Arrays),
  Slice = slice_local_array(Id, NSize, MSize, LocalArray),
  Slice.

store_neighbour_sliced_state(Id, SlicedState, Board = #board{arrays = Arrays}) ->
  Board#board{arrays = maps:put(Id, SlicedState, Arrays)}.

transform(Fun, Board) when is_function(Fun, 3) ->
  transform(
    fun
      (CellValue, Coords, Acc, Board) ->
        {Fun(CellValue, Coords, Board), Acc}
    end,
    no_acc,
    Board).

transform(Fun, Acc, Board = #board{n = NSize, m = MSize}) when is_function(Fun, 4) ->
  {Transformed, _} = lists:foldl(
    fun
      (M, MAcc) ->
        lists:foldl(
          fun
            (N, {Cells, FunAcc}) ->
              CellValue = get(N, M, Board),
              {Cell, NewFunAcc} = Fun(CellValue, {N, M}, FunAcc, Board),
              {[Cell | Cells], NewFunAcc}
          end,
          MAcc,
          lists:seq(0, NSize - 1))
    end,
    {[], Acc},
    lists:seq(0, MSize - 1)),
  from_list(NSize, MSize, lists:reverse(Transformed)).

slice_local_array(SliceId, NSize, MSize, Array) ->
  CoordList = coords_slice(SliceId, NSize, MSize),
  array:from_list([array:get(N, Array) || N <- CoordList]).

%% north edge
coords_slice({0, 1}, NSize, MSize) ->
  [N || N <- lists:seq((MSize - 1) * NSize, (MSize - 1) * NSize + NSize - 1)];
%% south edge
coords_slice({0, -1}, NSize, _MSize) ->
  [N || N <- lists:seq(0, NSize - 1)];
%% east edge
coords_slice({1, 0}, NSize, MSize) ->
  [NSize * M - 1 || M <- lists:seq(1, MSize)];
%% west edge
coords_slice({-1, 0}, NSize, MSize) ->
  [(NSize) * M || M <- lists:seq(0, MSize - 1)];
%% entire board
coords_slice({0, 0}, NSize, MSize) ->
  [X || X <- lists:seq(0, NSize * MSize - 1)];
%% corners
coords_slice({XId, YId}, NSize, MSize) ->
  [Xm, Ym] = lists:map(fun(-1) -> 0; (1) -> 1 end, [XId, YId]),
  [Ym * ((MSize - 1) * NSize) + Xm * (NSize - 1)].

translate_to_array_id(N, NSize, M, MSize) ->
  NId = neighbour_coord(N, NSize),
  MId = neighbour_coord(M, MSize),
  {NId, MId}.

neighbour_coord(-1, _XSize) -> 1; % next to 0
neighbour_coord(XSize, XSize) -> -1; % next to XSize - 1
neighbour_coord(X, XSize) when X >= 0, X =< (XSize - 1) -> 0. % within board range

coords_to_index({0, 0}, N, NSize, M, _MSize) -> M * NSize + N; % local board
coords_to_index({0, _MId}, N, _NSize, _M, _MSize) -> N;  % north or south neighbour edge
coords_to_index({_NId, 0}, _N, _NSize, M, _MSize) -> M; % west or east neighbour edge
coords_to_index({_NId, _MId}, _N, _NSize, _M, _MSize) -> 0. % neighbour corner


