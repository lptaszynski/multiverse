-module(game_of_life).

%% API
-export([
  register_world/0, register_world/1, register_world/2, register_world/3,
  demo/0, print/1, print/2, print/3, print/4, print_loop/5, print_to_console/0,
  print_to_console/1, print_to_file/1,
  start_chunk/2, start_chunks/0, start_chunks/1, get_chunk/1]).

-include("game_of_life.hrl").

-define(WORLD, ?MODULE).
-define(APP, ?MODULE).
-define(SLEEP, 100).

-define(N(WorlName), element(1, multiverse_world_config:get_metadata(WorlName, array_size))).
-define(M(WorlName), element(2, multiverse_world_config:get_metadata(WorlName, array_size))).

-define(DEFAULT_CONFIG, [
  {tick_delay, 100}, % interval between tick in miliseconds
  {links, hard}, % expectations and deliveries must be fulfilled to proceed
  {chunk_mod, game_of_life_chunk}, % chunk callback module
  {proxy_mod, game_of_life_proxy},
  {update_method, multicast}%, multicast_group, individual]}
]).

-define(DEFAULT_METADATA, [
  {array_size, {8, 8}}, % 8x8 cells in chunk
  {board_range, {{1, 4}, {1, 3}}}, % ranges of chunks
  {wrap, true} % wrap edges and corners
]).

register_world() -> register_world(?WORLD).
register_world(WorldName) -> register_world(WorldName, ?DEFAULT_CONFIG).
register_world(WorldName, Config) -> register_world(WorldName, Config, ?DEFAULT_METADATA).
register_world(WorldName, UserConfig, Metadata) ->
  [SortedUserConfig, SortedDefaultConfig] = [lists:ukeysort(1, L) || L <- [UserConfig, ?DEFAULT_CONFIG]],
  MergedConfig = lists:ukeymerge(1, SortedUserConfig, SortedDefaultConfig),
  [SortedMetadata, SortedDefaultMetadata] = [lists:ukeysort(1, L) || L <- [Metadata, ?DEFAULT_METADATA]],
  MergedMetadata = lists:ukeymerge(1, SortedMetadata, SortedDefaultMetadata),
  multiverse:register(WorldName, MergedConfig, MergedMetadata).

demo() ->
  register_world(),
  start_chunks(?WORLD),
  print_to_console(?WORLD).

start_chunks() -> start_chunks(?MODULE).

start_chunks(World) ->
  {{XMin, XMax}, {YMin, YMax}} = multiverse_world_config:get_metadata(World, board_range),
  io:format("starting chunks for ~p world~n", [World]),
  [start_chunk(World, {X, Y}) || X <- lists:seq(XMin, XMax), Y <- lists:seq(YMin, YMax)],
  ok.

start_chunk(World, Id) ->
  {ok, Board} = file:consult(File = chunk(Id)),
  io:format("chunk: ~p ~p loaded from file: ~p~n", [World, Id, File]),
  multiverse:start_chunk(World, Id, Board).

chunk({X, Y}) when (X rem 2) == 0, (Y rem 2) == 0 ->
  "../../priv/spaceship";

chunk({X, Y}) when (X rem 2) == 1, (Y rem 2) == 1 ->
  "../../priv/spaceship2";

chunk({_, _}) ->
  "../../priv/empty_board".

get_chunk(Id) ->
  multiverse:get_chunk_state(?WORLD, Id).

print_to_console() ->
  print(standard_io, ?WORLD).

print_to_console(World) ->
  print(standard_io, World).

print_to_file(FileName) ->
  spawn(
    fun() ->
      {ok, Fd} = file:open(FileName, [write]),
      print(Fd, ?WORLD)
    end).

print(WorldName) ->
  print(standard_io, WorldName).

print(IODevice, WorldName) ->
  {XRange, YRange} = multiverse_world_config:get_metadata(WorldName, board_range),
  print(XRange, YRange, IODevice, WorldName).

print({XMin, XMax}, {YMin, YMax}, WorldName) ->
  print({XMin, XMax}, {YMin, YMax}, standard_io, WorldName).

print({XMin, XMax}, {YMin, YMax}, IODevice, WorldName) ->
  spawn(?MODULE, print_loop, [{XMin, XMax}, {YMin, YMax}, IODevice, WorldName, 1]).

%% TODO refactor that demo spaghetti...
print_loop({XMin, XMax}, {YMin, YMax}, IODevice, WorldName, N) ->
  ArrayUnavailable = array:new(?M(WorldName) * ?N(WorldName), {default, -1}),
  ChunkUnavailable = chunk_unavailable,
  TRef = erlang:start_timer(?SLEEP, self(), skip),

  %% Making requests
  Requests = lists:foldl(
    fun(Y, YAcc) ->
      lists:foldl(
        fun(X, XAcc) ->
          {ok, Ref} = multiverse:get_chunk_state(WorldName, {X, Y}),
          maps:put(Ref, {X, Y}, XAcc)
        end,
        YAcc,
        lists:seq(XMin, XMax))
    end,
    #{},
    lists:seq(YMin, YMax)),

  %% collecting responses
  Chunks = (fun Loop(ChAcc) ->
    receive
      {timeout, TRef, skip} ->
        ChAcc;
      {_, {ok, not_found, {_, Node}, Ref}} ->
        case maps:find(Ref, Requests) of
          error -> Loop(ChAcc);
          {ok, ChId} -> Loop(maps:put(ChId, {Node, ArrayUnavailable}, ChAcc))
        end;
      {_, {ok, Response, {_, Node}, Ref}} ->
        case maps:find(Ref, Requests) of
          error -> Loop(ChAcc);
          {ok, ChId} -> Loop(maps:put(ChId, {Node, Response}, ChAcc))
        end;
      stop ->
        io:format("stopped printing ~p~n", [self()]),
        exit(normal);
      Msg ->
        io:format("unhandled, ~p~n", [Msg]),
        Loop(ChAcc)
    end
  end)(#{}),

  %% printing responses
  Frame = lists:foldl(
    fun(Y, Acc) ->
      Nodes = [lists:foldl(
        fun(X, NodesAcc) -> % node info
          {Node, _Array} = maps:get({X, Y}, Chunks, {ChunkUnavailable, ArrayUnavailable}),
          StrNode = atom_to_list(Node),
          [NodesAcc, StrNode, lists:duplicate((length(cell(0)) * (?N(WorldName) + 1) - length(atom_to_list(Node))
          ), $\s)]
        end,
        "",
        lists:seq(XMin, XMax)), [$\n]],

      YChunks = lists:foldl(
        fun(YPos, YAcc) ->
          Row = lists:foldl(
            fun(X, XAcc) ->
              {_Node, Array} = maps:get({X, Y}, Chunks, {ChunkUnavailable, ArrayUnavailable}),
              List = [array:get(YPos * ?N(WorldName) + XPos, Array) || XPos <- lists:seq(0, ?N(WorldName) - 1)],
              Chunk = lists:foldl(fun(Cell, ChAcc) -> [ChAcc, cell(Cell)] end, "", List),
              [XAcc, Chunk, "   "]
            end,
            "",
            lists:seq(XMin, XMax)),
          [YAcc, Row, [$\n]]
        end,
        "",
        lists:seq(?M(WorldName) - 1, 0, -1)),
      [Acc, Nodes, YChunks]
    end,
    "",
    lists:seq(YMax, YMin, -1)),
  io:format(IODevice, "~s", [[$\n] ++ "frame " ++ integer_to_list(N) ++ [$\n] ++ lists:flatten(Frame)]),
  %% looping
  print_loop({XMin, XMax}, {YMin, YMax}, IODevice, WorldName, N + 1).

cell(?DEAD) -> "[ ]";
cell(?ALIVE) -> "[*]";
cell(_) -> "[X]".