-module(multiverse_manager).

-behaviour(gen_server).

%% API
-export([
  start_link/0,
  register/3
]).

%% gen_server callbacks
-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {reg_worlds = sets:new() :: sets:set()}).

%% TODO Server should be responsible for (un)registering, starting, stoping worlds.
%% TODO for handling failures, rollbacking etc, each running world should have its own manager (with callbacks?)
%% API

start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

register(WorldName, WorldConfig, Metadata) ->
  gen_server:call(?SERVER, {register, WorldName, WorldConfig, Metadata}).


%% gen_server callbacks

init([]) ->
  process_flag(trap_exit, true),
  {ok, #state{reg_worlds = sets:new()}}.

handle_call({register, WorldName, Config, Metadata}, _From, State = #state{reg_worlds = Worlds}) ->
  multiverse_world_config:set_config(WorldName, Config),
  multiverse_world_config:set_metadata(WorldName, Metadata),
  lager:info("Registered world: ~p~nConfig: ~p~nMetadata: ~p~n", [WorldName, Config, Metadata]),
  {reply, ok, State#state{reg_worlds = sets:add_element(WorldName, Worlds)}};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


%% Internal functions

