-module(multiverse_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init(_Args) ->
  RWManager = {multiverse_manager,
    {multiverse_manager, start_link, []},
    permanent, 5000, worker, [multiverse_manager]},

  VNodeMaster = {multiverse_vnode_master,
    {riak_core_vnode_master, start_link, [multiverse_vnode]},
    permanent, 5000, worker, [riak_core_vnode_master]},
  ChunkVNodeMaster = {multiverse_chunk_vnode_master,
    {riak_core_vnode_master, start_link, [multiverse_chunk_vnode]},
    permanent, 5000, worker, [riak_core_vnode_master]},

  {ok,
    {{one_for_one, 5, 10},
      [VNodeMaster, ChunkVNodeMaster, RWManager]}}.
