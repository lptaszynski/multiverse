-module(multiverse).

-include("multiverse.hrl").
-export_type([client/0, world_name/0, chunk_id/0, chkey/0, tick/0, world_metadata/0, world_config/0, update_method/0]).

-include_lib("riak_core/include/riak_core_vnode.hrl").

-export([
  ping/0,
  register/2,
  register/3,
  start_chunk/3,
  stop_chunk/2,
  get_chunk_state/2
]).

-ignore_xref([
  ping/0
]).

%% Public API

%% @doc Pings a random vnode to make sure communication is functional
ping() ->
  DocIdx = riak_core_util:chash_key({<<"ping">>, term_to_binary(now())}),
  PrefList = riak_core_apl:get_primary_apl(DocIdx, 1, multiverse),
  [{IndexNode, _Type}] = PrefList,
  riak_core_vnode_master:sync_spawn_command(IndexNode, ping, multiverse_vnode_master).

%% TODO should be list of callback services and options
-spec register(world_name(), world_config()) -> ok | {error, Reason :: any()}.
register(WorldName, WorldConfig) ->
  register(WorldName, WorldConfig, []).
register(WorldName, WorldConfig, WorldMetadata) ->
  multiverse_manager:register(WorldName, WorldConfig, WorldMetadata).

start_chunk(World, ChunkId, Data) ->
  multiverse_chunk_vnode:start_chunk(World, ChunkId, Data).

stop_chunk(World, ChunkId) ->
  multiverse_chunk_vnode:stop_chunk(World, ChunkId).

get_chunk_state(World, ChunkId) ->
  multiverse_chunk_vnode:get_chunk_state(World, ChunkId).