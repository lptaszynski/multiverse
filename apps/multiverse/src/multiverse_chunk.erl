-module(multiverse_chunk).

-include("multiverse.hrl").

%% TODO Implement replication and master repair

%% TODO rethink: splitting chunk state into process specific state and chunk state for clarity?


%% ENCODING / DECODING

%% @doc Chunk state for backup - only crucial data. may be called in any time
-callback persist_state(ChunkProcessState :: any()) ->
  {ok, ChunkPersistData :: any()}.

%% @doc Chunk state for migration. may be called in any time
%% no subscriptions will be dropped during migration, so this should include eventual:
%% clients perspectives, events, stored neigbhour fragments etc.
%% after prepare/1 -> init_persisted(ChunkMigrateData, State) it must be a logical copy of ChunkProcessState
%% and be able to continue the same way.
%% @end
-callback migrate_state(ChunkProcessState :: any()) ->
  {ok, ChunkMigrateData :: chunk_data()}.


%% INITIALIZATION

%% @doc when handoff occurs, prepare is done before persisted/migrated data injection,
%% so it's good place to initialize heavy resources like external vm
%% @end
-callback prepare_resources(ChKey :: chkey()) ->
  {ok, ProcessState :: any()}.

%% @doc chunk initialization from state returned in persist_state/1.
-callback init_persisted(ChunkData :: chunk_data(), ProcessState :: any()) ->
  {ok, ChunkProcessState :: any()}.

%% @doc chunk initialization from state returned in migrate_state/1.
-callback init_migrated(ChunkData :: chunk_data(), ProcessState :: any()) ->
  {ok, ChunkProcessState :: any()}.


%% CLIENT INPUT.

%% @doc client event
-callback handle_event(Event :: any(), Client :: client(), ChunkProcessState :: any()) ->
  {reply, Reply :: any(), NewChunkProcessState :: any()} |
  {noreply, NewProcessChunkState :: any()}.


%% CLIENT OUTPUT.
%% @doc get request
-callback handle_get_request(GetRequest :: any(), Client :: client(), ChunkProcessState :: any()) ->
  {ok, Response :: any(), NewChunkProcessState :: any()}.

%% @doc called when client subscribes for updates
%% TODO support multicast groups and individual updates
%% @end
-callback client_subscribe(Client :: client(), ChunkProcessState :: any()) ->
  {allow, SyncData :: any(), NewChunkProcessState :: any()} |
  {disallow, Reason :: any(), NewChunkProcessState :: any()}.

%% @doc called when client unsubscribes
-callback client_unsubscribe(Client :: client(), Reason :: any(), ChunkProcessState :: any()) ->
  {ok, NewChunkProcessState :: any()}.

-callback update_clients({multicast, [Client :: client()]}, ChunkProcessState :: any()) ->
  {ok, Update :: any(), NewChunkProcessState :: any()} |
  {ok, Update :: any(), [{unsubscribe | skip, Client :: client() | [Client :: client()]}], NewChunkProcessState :: any()}.

%% INTERNAL CHUNKS COMMUNICATION

%% @doc directed links connecting chunks. Consistent computation is mandatory!
%% if and only if chunk A expects to receive data D in tick T from chunk B,
%% chunk B must declare to deliver data D to chunk A at the tick sequence independently.
%% if not and links are specified as "hard" then lock occurs.
%% @end
-callback sync_data_links(N :: tick(), Id :: any(), ChunkProcessState :: any()) ->
  {
    ok,
    {deliver, [{DestChunkId :: any(), DeliverDataId :: any()}]},
    {expect, [{SourceChunkId :: any(), ExpectedDataId :: any()}]},
    NewChunkProcessState :: any()
  }.
%% TODO implement linking request - need another callback
%% on link request A should freeze; (send request to B with T[A])*;
%% wait for reply from B which should advance to T[A] if needed; freeze and reply its current T[B2])* ;
%% (send agreement)* and A continue to T[B2] if higher - both linked in the same T[B2].

%% @doc data to deliver is extracted here.
%% pass function to send updates only. if some linked chunk got out of sync this might be called again,
%% so chunk should be able to deliver after updates after tick and with entire data state at any time
%% @end
-callback extract_sync_data({DestinationChunkId :: any(), FragmentId :: any()}, ChunkProcessState :: any()) ->
  {ok, StateFragmentData :: any(), ChunkProcessState :: any()} |
  {ok, UpdateFun :: function(), StateFragmentData :: any(), ChunkProcessState :: any()}.

%% @doc expected data is received.
%% If function is passed then next time for the same {SourceId, DataId} this function will be called instead
%% This is intended to sync data when needed and then apply updates only.
%% if delivering linked chunk gets out of sync, this might be called again, and coresponding data should be replaced
%% @end
-callback receive_sync_data({SourceChunkId :: any(), DataId :: any()}, Data :: any(), ChunkProcessState :: any()) ->
  {ok, NewChunkProcessState :: any()} |
  {ok, UpdateFun :: function(), NewChunkProcessState :: any()}.


%% STATE TRANSITIONS

%% @doc state transition
-callback tick(N :: tick(), ChunkProcessState :: any()) ->
  {ok, NewChunkProcessState :: any()}.


%% ASYNC PROCESS COMMUNICATION

%% @doc receive raw messages. Allows communication with external processes, ports etc.
-callback handle_info(Message :: any(), ChunkProcessState :: any()) ->
  {ok, NewChunkProcessState :: any()}.


%% TERMINATE

%% @doc gently free resources here
-callback terminate(Reason :: any(), ChunkProcessState :: any()) ->
  ok.