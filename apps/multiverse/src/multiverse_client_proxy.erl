-module(multiverse_client_proxy).

%% API
-export([subscribe/3, unsubscribe/3, event/4, get_request/4, heart/3]).

-include("multiverse.hrl").

-callback multicast(World :: world_name(), ChunkId :: any(), Tick :: tick(), Clients :: [client()], Data :: any()) -> ok.

subscribe(World, ChunkId, Client) ->
  {ok, _Ref} = multiverse_chunk_fsm:client_subscribe({World, ChunkId}, Client).

unsubscribe(World, ChunkId, Client) ->
  {ok, _Ref} = multiverse_chunk_fsm:client_unsubscribe({World, ChunkId}, Client).

event(World, ChunkId, Client, EventData) ->
  {ok, _Ref} = multiverse_chunk_fsm:client_event({World, ChunkId}, Client, EventData).

get_request(World, ChunkId, Client, RequestData) ->
  {ok, _Ref} = multiverse_chunk_fsm:client_event({World, ChunkId}, Client, RequestData).

heart(World, ChunkId, Client) ->
  {ok, _Ref} = multiverse_chunk_fsm:client_heart({World, ChunkId}, Client).
