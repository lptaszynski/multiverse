%% @doc A vnode to handle chunks execution
-module(multiverse_chunk_vnode).
-behaviour(riak_core_vnode).
-include("multiverse.hrl").
-include_lib("riak_core/include/riak_core_vnode.hrl").
-export([
  get_chunk_state/2,
  start_vnode/1,
  init/1,
  terminate/2,
  handle_command/3,
  is_empty/1,
  delete/1,
  handle_handoff_command/3,
  handoff_starting/2,
  handoff_cancelled/1,
  handoff_finished/2,
  handle_handoff_data/2,
  encode_handoff_item/2,
  handle_coverage/4,
  handle_info/2,
  handle_exit/3]).

-export([
  start_chunk/3,
  stop_chunk/2,
  prepare_chunks_for_handoff/2,
  make_chunk_command/2,
  make_chunk_command/3,
  make_raw_chunk_command/2,
  forward_to_chunk/2
]).

-record(state, {partition, node, chunks = dict:new()}).

-record(chunk_info, {
  pid :: pid(),
  state :: preparing | prepared | initializing | ready | handoff,
  data :: binary(),
  chunk_commands = [] :: [any()]}). %% stored commands until chunk fsm gets ready

-record(chunk_command, {
  command_data :: any(),
  reply_fun :: undefined | function()}).

-record(forward, {
  event
}).

-define(MASTER, multiverse_chunk_vnode_master).

%% API

get_chunk_state(World, ChunkId) ->
  ReqId = mk_reqid(),
  riak_core_vnode_master:command(idx_node({World, ChunkId}),
    {get_chunk_state, {World, ChunkId}, ReqId},
    {raw, undefined, self()},
    ?MASTER),
  {ok, ReqId}.

start_vnode(I) ->
  riak_core_vnode_master:get_vnode_pid(I, ?MODULE).

%% TODO clean api
start_chunk(World, ChunkId, Data) ->
  DocIdx = riak_core_util:chash_key({World, term_to_binary(ChunkId)}),
  PrefList = riak_core_apl:get_primary_apl(DocIdx, 1, multiverse),
  [{IndexNode, _Type}] = PrefList,
  riak_core_vnode_master:sync_spawn_command(IndexNode, {start_chunk, {World, ChunkId}, Data}, ?MASTER).

stop_chunk(World, ChunkId) ->
  stop_chunk(World, ChunkId, undefined).

stop_chunk(World, ChunkId, Reason) ->
  DocIdx = riak_core_util:chash_key({World, term_to_binary(ChunkId)}),
  PrefList = riak_core_apl:get_primary_apl(DocIdx, 1, multiverse),
  [{IndexNode, _Type}] = PrefList,
  riak_core_vnode_master:sync_spawn_command(IndexNode, {stop_chunk, {World, ChunkId}, Reason}, ?MASTER).

prepare_chunks_for_handoff(Idx, Chunks) ->
  riak_core_vnode_master:command(Idx, {prepare_chunks_for_handoff, Chunks}, ignore, ?MASTER).

%% noreply command
make_chunk_command(ChKey, CommandData) ->
  riak_core_vnode_master:command(idx_node(ChKey),
    {chunk_command, ChKey, CommandData},
    ignore,
    ?MASTER).

%% reply command
make_chunk_command(ChKey, CommandData, ReqId) ->
  riak_core_vnode_master:command(idx_node(ChKey),
    {chunk_command, ChKey, CommandData, ReqId},
    {fsm, undefined, self()},
    ?MASTER).

make_raw_chunk_command({World, ChunkId}, CommandData) ->
  ReqId = mk_reqid(),
  riak_core_vnode_master:command(idx_node({World, ChunkId}),
    {chunk_command, {World, ChunkId}, CommandData, ReqId},
    {raw, undefined, self()},
    ?MASTER),
  {ok, ReqId}.

forward_to_chunk(ChKey, Event) ->
  riak_core_vnode_master:command(idx_node(ChKey),
    {forward_to_chunk, ChKey, Event},
    ignore,
    ?MASTER).


%% Callbacks

init([Partition]) ->
  lager:debug("VNODE INIT ~p ~p", [node(), Partition]),
  link(whereis(multiverse_manager)),
  {ok, #state{partition = Partition, node = node()}}.

handle_command({prepare_chunks_for_handoff, ChunksToPrepare}, _Sender, State0 = #state{chunks = Chunks0}) ->
  lager:debug("preparing_chunks_for_handoff ~p", [ChunksToPrepare]),
  Chunks1 = lists:foldl(
    fun(Ch, Acc0) ->
      {_, ChunksAcc} = prepare_chunk_procedure(Ch, Acc0),
      ChunksAcc
    end, Chunks0, ChunksToPrepare),
  {noreply, State0#state{chunks = Chunks1}};

handle_command({start_chunk, ChKey, ChunkData}, _Sender, State0 = #state{chunks = Chunks0, partition = Partition, node = Node}) ->
  ChunkPersistantData = multiverse_chunk_fsm:to_persistant(ChKey, 1, ChunkData),
  {Reply, Chunks1} = start_chunk_procedure(ChKey, ChunkPersistantData, Chunks0),
  {reply, {ok, Reply, {Partition, Node}}, State0#state{chunks = Chunks1}};

handle_command({stop_chunk, ChKey}, _Sender, State = #state{chunks = Chunks0, partition = Partition, node = Node}) ->
  Reply =
    case dict:find(ChKey, Chunks0) of
      error -> not_found;
      {ok, #chunk_info{pid = Pid}} -> multiverse_chunk_fsm:stop(Pid, reload)
    end,
  Chunks1 = dict:erase(ChKey, Chunks0),
  {reply, {ok, Reply, {Partition, Node}}, State#state{chunks = Chunks1}};

%% TODO debug command
handle_command({get_chunk_state, ChKey, ReqId}, _Sender, State = #state{chunks = Chunks0, partition = Partition, node = Node}) ->
  Reply = case dict:find(ChKey, Chunks0) of
            error -> not_found;
            {ok, #chunk_info{pid = Pid, state = ready}} ->
              try multiverse_chunk_fsm:get_chunk_state(Pid) of Response -> Response
              catch _:_ -> not_found end;
            {ok, #chunk_info{pid = Pid, state = _}} -> not_found

          end,
  {reply, {ok, Reply, {Partition, Node}, ReqId}, State};

handle_command({chunk_command, ChKey, CommandData, ReplyFun}, _Sender,
    State = #state{chunks = Chunks}) when is_function(ReplyFun) ->
  case dict:find(ChKey, Chunks) of
    error ->
      ReplyFun({vnode_response, not_found}),
      {noreply, State};
    {ok, #chunk_info{pid = Pid, state = ready}} ->
      multiverse_chunk_fsm:handle_command(Pid, CommandData, ReplyFun),
      {noreply, State};
    {ok, #chunk_info{pid = Pid, chunk_commands = StoredCommands} = ChInfo} ->
      ChunkCommand = #chunk_command{command_data = CommandData, reply_fun = ReplyFun},
      {noreply, State#state{chunks = dict:store(ChKey, ChInfo#chunk_info{chunk_commands = [ChunkCommand | StoredCommands]}, Chunks)}};
    {ok, Value} ->
      lager:error([{chkey, ChKey}], "~p Failed dict: ~p~nreturned value ~p~n", [ChKey, dict:to_list(Chunks), Value]),
      {noreply, State}
  end;

handle_command({chunk_command, ChKey, CommandData, ReqId} = C, Sender, State = #state{chunks = Chunks, partition = Partition, node = Node}) ->
  case dict:find(ChKey, Chunks) of
    error ->
      Response = multiverse_chunk_fsm:to_response(ReqId, {Partition, Node}, {vnode_response, not_found}),
      {reply, Response, State};
    {ok, #chunk_info{pid = Pid, state = ready}} ->
      multiverse_chunk_fsm:handle_command(Pid, CommandData, reply_fun(ReqId, Sender, {Partition, Node})),
      {noreply, State};
    {ok, #chunk_info{pid = Pid, chunk_commands = StoredCommands} = ChInfo} ->
      ChunkCommand = #chunk_command{command_data = CommandData, reply_fun = reply_fun(ReqId, Sender, {Partition, Node})},
      {noreply, State#state{chunks = dict:store(ChKey, ChInfo#chunk_info{chunk_commands = [ChunkCommand | StoredCommands]}, Chunks)}};
    {ok, Value} ->
      lager:error([{chkey, ChKey}], "~p Failed dict: ~p~nreturned value ~p~n", [ChKey, dict:to_list(Chunks), Value]),
      {noreply, State}
  end;

handle_command({chunk_command, ChKey, CommandData}, ignore, State = #state{chunks = Chunks}) ->
  case dict:find(ChKey, Chunks) of
    error ->
      {noreply, State};
    {ok, #chunk_info{pid = Pid, state = ready}} ->
      multiverse_chunk_fsm:handle_command(Pid, CommandData),
      {noreply, State};
    {ok, #chunk_info{pid = Pid, chunk_commands = StoredCommands} = ChInfo} ->
      ChunkCommand = #chunk_command{command_data = CommandData},
      {noreply, State#state{chunks = dict:store(ChKey, ChInfo#chunk_info{chunk_commands = [ChunkCommand | StoredCommands]}, Chunks)}}
  end;

handle_command({forward_to_chunk, ChKey, Event}, ignore, State = #state{chunks = Chunks}) ->
  case dict:find(ChKey, Chunks) of
    error ->
      {noreply, State};
    {ok, #chunk_info{pid = Pid, state = ready}} ->
      multiverse_chunk_fsm:forward_event(Pid, Event),
      {noreply, State};
    {ok, #chunk_info{pid = Pid, chunk_commands = StoredCommands} = ChInfo} ->
      ChunkCommand = #forward{event = Event},
      {noreply, State#state{chunks = dict:store(ChKey, ChInfo#chunk_info{chunk_commands = [ChunkCommand | StoredCommands]}, Chunks)}}
  end.

handle_handoff_command(?FOLD_REQ{foldfun = Fun, acc0 = Acc0}, _Sender, State) ->
  Acc1 = dict:fold(Fun, Acc0, State#state.chunks),
  {reply, Acc1, State};

handle_handoff_command({forward_to_chunk, _ChKey, _Event}, ignore, State) ->
  {forward, State};

handle_handoff_command({chunk_command, ChKey, CommandData}, _Sender, State = #state{chunks = Chunks}) ->
  case dict:find(ChKey, Chunks) of
    error ->
      {noreply, State};
    {ok, #chunk_info{pid = Pid}} ->
      case multiverse_chunk_fsm:in_handoff_state(Pid) of
        true ->
          {forward, State};
        false ->
          multiverse_chunk_fsm:handle_command(Pid, CommandData),
          {noreply, State}
      end;
    {ok, Value} ->
      lager:error([{chkey, ChKey}], "~p Failed dict: ~p~nreturned value ~p~n", [ChKey, dict:to_list(Chunks), Value]),
      {noreply, State}
  end;

handle_handoff_command({chunk_command, ChKey, CommandData, ReqId} = C, Sender, State = #state{chunks = Chunks, partition = Partition, node = Node}) ->
  case dict:find(ChKey, Chunks) of
    error ->
      Response = multiverse_chunk_fsm:to_response(ReqId, {Partition, Node}, {vnode_response, not_found}),
      {reply, Response, State};
    {ok, #chunk_info{pid = Pid}} ->
      case multiverse_chunk_fsm:in_handoff_state(Pid) of
        true ->
          {forward, State};
        false ->
          multiverse_chunk_fsm:handle_command(Pid, CommandData, reply_fun(ReqId, Sender, {Partition, Node})),
          {noreply, State}
      end;
    {ok, Value} ->
      lager:error([{chkey, ChKey}], "~p Failed dict: ~p~nreturned value ~p~n", [ChKey, dict:to_list(Chunks), Value]),
      {noreply, State}
  end;

handle_handoff_command(Command, _Sender, State) ->
  lager:error("unexpected handoff command ~n", [Command]),
  {forward, State}.

handoff_starting({_HOType, {_TargetIdx, _TargetNode}}, State = #state{node = Node, partition = Partition}) ->
  lager:debug([{node, Node}, {partition, Partition}], "handoff_starting, size: ~p partition: ~p", [dict:size(State#state.chunks), State#state.partition]),
  %% TODO send command self and then forward?
  %% prepare_chunks_resources(TargetIdx, dict:fetch_keys(State#state.chunks)),
  {true, State}.

handoff_cancelled(State = #state{chunks = Chunks, node = Node, partition = Partition}) ->
  lager:debug([{node, Node}, {partition, Partition}], "handoff_cancelled, size: ~p partition: ~p", [dict:size(State#state.chunks), State#state.partition]),
  dict:fold(fun(_, #chunk_info{pid = Pid}, _) -> multiverse_chunk_fsm:handoff_cancelled(Pid) end, ok, Chunks),
  {ok, State}.

handoff_finished(_TargetNode, State = #state{node = Node, partition = Partition}) ->
  lager:debug([{node, Node}, {partition, Partition}], "handoff_finished, size: ~p partition: ~p", [dict:size(State#state.chunks), State#state.partition]),
  {ok, State}.

handle_handoff_data(Data, State = #state{chunks = Chunks0}) ->
  {ChKey, ChunkFSMBinState} = binary_to_term(Data),
  {Reply, Chunks1} = start_chunk_procedure(ChKey, ChunkFSMBinState, Chunks0),
  lager:debug("handle_handoff_data, ~p ChKey: ~p partition: ~p", [Reply, ChKey, State#state.partition]),
  {reply, Reply, State#state{chunks = Chunks1}}.

encode_handoff_item(ChKey, #chunk_info{pid = Pid, state = ready}) ->
  {ok, EncodedData} = multiverse_chunk_fsm:handle_handoff(Pid),
  term_to_binary({ChKey, EncodedData}).

is_empty(State) ->
  lager:debug("is_empty size: ~p partition: ~p", [dict:size(State#state.chunks), State#state.partition]),
  {State#state.chunks == 0, State}.

delete(State) ->
  lager:debug("delete, size: ~p partition: ~p", [dict:size(State#state.chunks), State#state.partition]),
  {ok, State}.

handle_coverage(_Req, _KeySpaces, _Sender, State) ->
  {stop, not_implemented, State}.

handle_exit(Pid, Reason, _State) ->
  %% TODO should be recovered or cleaned
  lager:warning("fsm exits ~p ~p", [Pid, Reason]),
  {noreply, _State}.

handle_info({prepared, ChKey}, State = #state{chunks = Chunks0}) ->
  %% TODO request migration if handoff
  Chunks1 =
    case dict:find(ChKey, Chunks0) of
      {ok, ChInfo = #chunk_info{state = preparing, data = undefined}} ->
        dict:store(ChKey, ChInfo#chunk_info{state = prepared}, Chunks0);
      {ok, ChInfo = #chunk_info{pid = Pid, state = preparing, data = ChunkData}} ->
        multiverse_chunk_fsm:do_init(Pid, ChunkData),
        dict:store(ChKey, ChInfo#chunk_info{state = initializing}, Chunks0);
      Unhandled ->
        lager:error("Unhandled dict:find for ~p ~p", [ChKey, Unhandled]),
        Chunks0
    end,
  {ok, State#state{chunks = Chunks1}};

handle_info({initialized, ChKey}, State = #state{chunks = Chunks0}) ->
  {ok, ChInfo = #chunk_info{pid = Pid, chunk_commands = Commands}} = dict:find(ChKey, Chunks0),
  [forward_command(Pid, Req) || Req <- lists:reverse(Commands)],
  Chunks1 = dict:store(ChKey, ChInfo#chunk_info{state = ready, data = undefined, chunk_commands = []}, Chunks0),
  {ok, State#state{chunks = Chunks1}};

handle_info(_Message, State = #state{chunks = _Chunks}) ->
  {ok, State}.

terminate(_Reason, _State) ->
  ok.

reply_fun(ReqId, Sender, VNodeInfo) ->
  fun(Reply) -> riak_core_vnode:reply(Sender, multiverse_chunk_fsm:to_response(ReqId, VNodeInfo, Reply)) end.

forward_command(Pid, #forward{event = Event}) ->
  multiverse_chunk_fsm:forward_event(Pid, Event);
forward_command(Pid, #chunk_command{command_data = CommandData, reply_fun = undefined}) ->
  multiverse_chunk_fsm:handle_command(Pid, CommandData);
forward_command(Pid, #chunk_command{command_data = CommandData, reply_fun = ReplyFun}) ->
  multiverse_chunk_fsm:handle_command(Pid, CommandData, ReplyFun).


%% TODO remove duplication start_chunk_procedure
prepare_chunk_procedure(ChKey, Chunks0) ->
  {NeedStart, Chunks1} =
    case dict:find(ChKey, Chunks0) of
      error ->
        {true, Chunks0};
      {ok, #chunk_info{state = St, pid = Pid}} when (St == prepared) orelse (St == preparing) ->
        {false, Chunks0};
      {ok, #chunk_info{state = St, pid = Pid}} ->
        multiverse_chunk_fsm:stop(Pid, normal),
        {true, dict:erase(ChKey, Chunks0)} %% TODO this case should be arbitrated first...
    end,
  case NeedStart of
    true ->
      case multiverse_chunk_fsm:start_link(ChKey, master) of
        {ok, Pid2} ->
          NewChunkInfo = #chunk_info{pid = Pid2, state = preparing},
          {ok, dict:store(ChKey, NewChunkInfo, Chunks1)};
        Error ->
          lager:error("starting chunk failed ~p", [Error]),
          {{error, Error}, Chunks1}
      end;
    false ->
      {ok, Chunks1}
  end.


start_chunk_procedure(ChKey, ChunkData, Chunks0) ->
  {NeedStart, Chunks1} =
    case dict:find(ChKey, Chunks0) of
      error ->
        {true, Chunks0};
      {ok, ChInfo = #chunk_info{state = preparing, pid = Pid}} ->
        {false, dict:store(ChKey, ChInfo#chunk_info{data = ChunkData}, Chunks0)}; %% delay init phase
      {ok, ChInfo = #chunk_info{state = prepared, pid = Pid}} ->
        multiverse_chunk_fsm:do_init(Pid, ChunkData), %% apply init
        {false, dict:store(ChKey, ChInfo#chunk_info{state = initializing}, Chunks0)};
      {ok, #chunk_info{state = St, pid = Pid}} when (St == initializing) orelse (St == ready) ->
        multiverse_chunk_fsm:stop(Pid, normal), %% TODO this case should be arbitrated first...
        {true, dict:erase(ChKey, Chunks0)}
    end,
  case NeedStart of
    true ->
      case multiverse_chunk_fsm:start_link(ChKey, master) of
        {ok, Pid2} ->
          NewChunkInfo = #chunk_info{pid = Pid2, state = preparing, data = ChunkData},
          {ok, dict:store(ChKey, NewChunkInfo, Chunks1)};
        Error ->
          lager:error("starting chunk failed ~p", [Error]),
          {{error, Error}, Chunks1}
      end;
    false ->
      {ok, Chunks1}
  end.

idx_node({World, ChId} = ChKey) ->
  DocIdx = riak_core_util:chash_key({World, term_to_binary(ChId)}),
  PrefList = riak_core_apl:get_apl(DocIdx, 1, multiverse),
  case PrefList of
    [IdxNode] -> [IdxNode];
    [] ->
      lager:error("node unavailable for ~p", [ChKey]),
      [] %% TODO notify next one?
  end.

mk_reqid() -> erlang:phash2(erlang:now()).