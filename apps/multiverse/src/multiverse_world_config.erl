-module(multiverse_world_config).

%% API
-export([
  set_config/2, set_config/3, get_config/1, get_config/2,
  set_metadata/2, set_metadata/3, get_metadata/1, get_metadata/2
]).

-define(CONF(World), {World, config}).
-define(META(World), {World, metadata}).

%% configuration for multiverse
set_config(_World, []) -> ok;
set_config(World, [{Key, Value} | Tail]) ->
  riak_core_metadata:put(?CONF(World), Key, Value),
  set_config(World, Tail).

set_config(World, Key, Value) ->
  riak_core_metadata:put(?CONF(World), Key, Value).

get_config(World) ->
  riak_core_metadata:fold(fun({K, V}, Acc) -> [{K, V} | Acc] end, [], ?CONF(World)).

get_config(World, Key) ->
  riak_core_metadata:get(?CONF(World), Key).


%% internal app configuration.
set_metadata(_World, []) -> ok;
set_metadata(World, [{Key, Value} | Tail]) ->
  riak_core_metadata:put(?META(World), Key, Value),
  set_metadata(World, Tail).

set_metadata(World, Key, Value) ->
  riak_core_metadata:put(?META(World), Key, Value).

get_metadata(World) ->
  riak_core_metadata:fold(fun({K, V}, Acc) -> [{K, V} | Acc] end, [], ?META(World)).

get_metadata(World, Key) ->
  riak_core_metadata:get(?META(World), Key).
