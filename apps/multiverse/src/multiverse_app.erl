-module(multiverse_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
  case multiverse_sup:start_link() of
    {ok, Pid} ->
      ok = riak_core:register([{vnode_module, multiverse_vnode}]),
      ok = riak_core:register([{vnode_module, multiverse_chunk_vnode}]),

      ok = riak_core_ring_events:add_guarded_handler(multiverse_ring_event_handler, []),
      ok = riak_core_node_watcher_events:add_guarded_handler(multiverse_node_event_handler, []),
      ok = riak_core_node_watcher:service_up(multiverse, self()),

      {ok, Pid};
    {error, Reason} ->
      {error, Reason}
  end.

stop(_State) ->
  ok.
