-module(multiverse_chunk_fsm).

-behaviour(gen_fsm).

-include("multiverse.hrl").

%% API
-export([
  get_chunk_state/1,
  start_link/2,
  stop/1,
  stop/2,
  do_init/2,
  persist_chunk_state/1,
  handle_command/2,
  handle_command/3,
  make_command/2,
  make_command/3,
  forward_event/2,
  handle_handoff/1,
  handoff_cancelled/1,
  in_handoff_state/1,
  to_persistant/3,
  to_response/3
]).

% client proxy api
-export([
  client_subscribe/2,
  client_unsubscribe/2,
  client_event/3,
  client_get_request/3,
  client_heart/2]).

%% gen_fsm callbacks
-export([
  init/1,
  handle_event/3,
  handle_sync_event/4,
  handle_info/3,
  terminate/3,
  code_change/4]).

%% gen_fsm states
-export([
  preparing_env/2,
  awaiting_state/2,
  ready/2,
  handoff/2
]).

-define(CLIENT_HEART_TIMEOUT, (15 + 1) * 1000). % TODO fix heart

-define(CONF(Key, ChKey), multiverse_world_config:get_config(element(1, ChKey), Key)).
-define(TICK_DELAY(ChKey), ?CONF(tick_delay, ChKey)).
-define(LINKS_TYPE(ChKey), ?CONF(links_type, ChKey)).


%% TODO efficient record encoding/decoding.
-record(command, {
  comm_data :: term(),
  reply_fun :: noreply | function()}).
% reply function sends back #response with proper reqid, vnode info and its one argument as resp_data.

-record(command_stamp, {
  dest_chkey,
  comm_data %% TODO for eventual retransmition. not sure about that, function maybe?
}).

-record(response, {
  req_id,
  vnode_info,
  resp_data :: term()
}).

-record(delivery, {
  tick :: tick(),
  type :: sync | update,
  source_chunk_id,
  data_id,
  data :: term()}).

-record(delivery_ack, {}).

-record(delivery_err, {
  reason :: undefined | need_sync
}).

-record(state, {
  chkey :: chkey(),
  chunk_mod :: module(),
  proxy_mod :: module(),
  update_method = multicast :: multicast,
  tick :: non_neg_integer(),
  update_extract_fun,
  update_receive_fun,
  parent :: pid(),
  retries = 0 :: non_neg_integer(), % soft links only
  deliver_sd :: #{}, %% sd - short of sync data
  expected_sd :: sets:set(),
  received_future_sd :: #{},
  applied_sd :: #{}, % for eventual ack
  subscriptions = #{} :: #{},
  chunk_state :: term()
}).

-record(handoff_state, {
  tick :: non_neg_integer(),
  update_extract_fun,
  update_receive_fun,
  deliver_sd :: #{}, %% sd - sync data
  expected_sd :: sets:set(),
  received_future_sd :: #{},
  applied_sd :: #{}, % for eventual ack
  subscriptions = #{} :: #{},
  chunk_data :: any() %% used for handoff
}).

-record(persistent_state, {
  chkey :: chkey(),
  tick = 1 :: non_neg_integer(),
  chunk_data :: any()
}).

-record(client_event, {name, client, data}).
-record(subscription, {ts, tref}).

%% API

get_chunk_state(Pid) ->
  gen_fsm:sync_send_all_state_event(Pid, get_chunk_state).

start_link({World, ChunkId}, Opts) ->
  gen_fsm:start_link(?MODULE, [self(), {World, ChunkId}, Opts], []).

stop(Pid) ->
  stop(Pid, undefined).

stop(Pid, Reason) ->
  gen_fsm:send_all_state_event(Pid, {stop, Reason}).

do_init(Pid, ChunkData) ->
  gen_fsm:send_event(Pid, {do_init, ChunkData}).

persist_chunk_state(Pid) ->
  gen_fsm:sync_send_all_state_event(Pid, persist_chunk_state).

handle_command(Pid, CommandData) ->
  gen_fsm:send_event(Pid, #command{comm_data = CommandData}).

handle_command(Pid, CommandData, ReplyFun) ->
  gen_fsm:send_event(Pid, #command{comm_data = CommandData, reply_fun = ReplyFun}).

forward_event(Pid, Event) ->
  gen_fsm:send_event(Pid, Event).

to_persistant(ChKey, Tick, ChunkData) -> %% TODO shouldn't be here
  #persistent_state{chkey = ChKey, tick = Tick, chunk_data = ChunkData}.

to_response(ReqId, VNodeInfo, Response) ->
  #response{req_id = ReqId, vnode_info = VNodeInfo, resp_data = Response}.

%% client proxy api
client_subscribe({World, ChunkId}, Client) ->
  multiverse_chunk_vnode:make_raw_chunk_command({World, ChunkId},
    #client_event{name = subscribe, client = Client}).

client_unsubscribe({World, ChunkId}, Client) ->
  multiverse_chunk_vnode:make_raw_chunk_command({World, ChunkId},
    #client_event{name = unsubscribe, client = Client}).

client_event({World, ChunkId}, Client, EventData) ->
  multiverse_chunk_vnode:make_raw_chunk_command({World, ChunkId},
    #client_event{name = event, client = Client, data = EventData}).

client_get_request({World, ChunkId}, Client, RequestData) ->
  multiverse_chunk_vnode:make_raw_chunk_command({World, ChunkId},
    #client_event{name = get_request, client = Client, data = RequestData}).

client_heart({World, ChunkId}, Client) ->
  multiverse_chunk_vnode:make_raw_chunk_command({World, ChunkId},
    #client_event{name = heart, client = Client}).


%% handoff api
handle_handoff(Pid) ->
  gen_fsm:sync_send_all_state_event(Pid, handle_handoff).

in_handoff_state(Pid) ->
  gen_fsm:sync_send_all_state_event(Pid, in_handoff_state).

handoff_cancelled(Pid) ->
  gen_fsm:sync_send_all_state_event(Pid, handoff_cancelled).

%% CALLBACKS
init([ParentPid, ChKey, _Opts]) ->
  {ok, preparing_env, #state{parent = ParentPid, chkey = ChKey}, 0}.

preparing_env(timeout, State = #state{parent = ParentPid, chkey = ChKey}) ->
  Mod = ?CONF(chunk_mod, ChKey),
  Proxy = ?CONF(proxy_mod, ChKey),
  {ok, ChunkState} = Mod:prepare_resources(ChKey),
  ParentPid ! {prepared, ChKey},
  {next_state, awaiting_state, State#state{chunk_mod = Mod, proxy_mod = Proxy, chunk_state = ChunkState}}.

awaiting_state({do_init, ChunkFSMState},
    State0 = #state{parent = ParentPid, chunk_mod = Mod, chkey = ChKey, chunk_state = ChunkState0}) ->
  State2 =
    case ChunkFSMState of
      #handoff_state{chunk_data = ChunkData} = HandoffState ->
        lager:debug([{chkey, ChKey}], "chunk ~p fsm got handoff data~n", [ChKey]),
        State1 = handoff_state_to_state(HandoffState, State0),
        {ok, ChunkState1} = Mod:init_migrated(ChunkData, State1#state.chunk_state),
        ParentPid ! {initialized, ChKey},
        State1#state{
          chunk_state = ChunkState1};
      #persistent_state{tick = Tick, chunk_data = ChunkData} ->
        {ok, ChunkState1} = Mod:init_persisted(ChunkData, ChunkState0),
        ParentPid ! {initialized, ChKey},
        State1 = State0#state{
          tick = Tick,
          update_method = ?CONF(update_method, ChKey),
          chunk_state = ChunkState1,
          update_extract_fun = undefined,
          update_receive_fun = undefined,
          received_future_sd = #{},
          applied_sd = #{}
        },
        #state{} = pre_tick(State1)
    end,
  gen_fsm:send_event_after(?TICK_DELAY(ChKey), try_tick),
  {next_state, ready, State2}.

%% TODO improve tick delay synchronization
ready(try_tick, State0 = #state{chunk_mod = Mod, retries = Retries,
  tick = Tick, chkey = ChKey, chunk_state = ChunkState0}) ->
  AreDeliveriesFulfilled = are_deliveres_fulfilled(State0#state.deliver_sd),
  AreExpectationsFulfilled = are_expectations_fulfilled(State0#state.expected_sd),
  case (AreExpectationsFulfilled and AreDeliveriesFulfilled) orelse (?LINKS_TYPE(ChKey) == soft)
  of
    true ->%% TODO really blocking block - split it?
      gen_fsm:send_event_after(?TICK_DELAY(ChKey), try_tick),
      {ok, ChunkState1} = Mod:tick(Tick, ChunkState0),
      State1 = State0#state{tick = Tick + 1, chunk_state = ChunkState1},
      UpdateResult = Mod:update_clients(
        {multicast, maps:keys(State1#state.subscriptions)}, ChunkState1), %% TODO multicast only for now
      State2 = handle_update_clients_result(UpdateResult, State1),
      State3 = pre_tick(State2),
      {next_state, ready, State3#state{retries = 0}};
    false ->
      lager:debug([{chkey, ChKey}], "chunk ~p no ack for deliveries: ~p", [ChKey, maps:to_list(State0#state.deliver_sd)]),
      lager:debug([{chkey, ChKey}], "chunk ~p no fulfilled expectation: ~p", [ChKey, sets:to_list(State0#state.expected_sd)]),
      maps:fold(fun(ReqId, Command, _) -> retry_command(ReqId, Command) end, noacc, State0#state.deliver_sd),
      gen_fsm:send_event_after(?TICK_DELAY(ChKey) div 2, try_tick), % TODO temporary solution
      {next_state, ready, State0#state{retries = Retries + 1}}
  end;

%% chunk not found...
ready(#response{req_id = ReqId, resp_data = {vnode_response, not_found}} = R,
    State0 = #state{chkey = ChKey, tick = Tick, deliver_sd = Deliveries0}) ->
  lager:debug([{chkey, State0#state.chkey}], "in ~p received ~p", [Tick, R]),
  case was_sended(ReqId, Deliveries0) of
    true ->
      %% TODO where to recover it?
      {next_state, ready, State0};
    false ->
      lager:debug([{chkey, ChKey}], "~p unexpected response in ready ~p", [ChKey, R]),
      {next_state, ready, State0}
  end;

%% handle acknowledgment for delivery
ready(#response{req_id = ReqId, vnode_info = _VNodeInfo, resp_data = #delivery_ack{}} = R,
    State = #state{chkey = ChKey, tick = Tick, deliver_sd = Deliveries0}) ->
  lager:debug([{chkey, State#state.chkey}], "in ~p received ~p", [Tick, R]),
  case was_sended(ReqId, Deliveries0) of
    true ->
      Deliveries1 = mark_as_delivered(ReqId, Deliveries0),
      {next_state, ready, State#state{deliver_sd = Deliveries1}};
    false ->
      lager:debug([{chkey, ChKey}], "~p unexpected response in ready ~p", [State#state.chkey, R]),
      {next_state, ready, State}
  end;

%% destination chunk is out of sync
ready(#response{req_id = ReqId, vnode_info = _VNodeInfo, resp_data = #delivery_err{reason = need_sync}} = R,
    State0 = #state{chkey = ChKey, deliver_sd = Deliveries0}) ->
  case was_sended(ReqId, Deliveries0) of
    true ->
      #command_stamp{dest_chkey = DestChunkId, comm_data = #delivery{data_id = DataId}} = maps:get(ReqId, Deliveries0),
      Deliveries1 = maps:remove(ReqId, Deliveries0),
      State1 = make_new_delivery({DestChunkId, DataId}, State0#state{update_extract_fun = undefined, deliver_sd = Deliveries1}),
      {next_state, ready, State1};
    false ->
      lager:debug([{chkey, ChKey}], "~p unexpected response in ready ~p", [State0#state.chkey, R]),
      {next_state, ready, State0}
  end;

%% handle delivery for current tick
ready(#command{comm_data = Delivery = #delivery{source_chunk_id = SourceChunkId, data_id = DataId, tick = DTick, type = Type}} = C,
    State0 = #state{chkey = ChKey, tick = Tick, expected_sd = ExpectedSD0, applied_sd = AppliedSD0}) when DTick == Tick ->
  lager:debug([{chkey, State0#state.chkey}], "received delivery ~p", [C]),
  State2 = case sets:is_element({SourceChunkId, DataId}, ExpectedSD0) of
             true ->
               case can_delivery_be_applied(Type, State0#state.received_future_sd) of
                 true ->
                   State1 = apply_delivery(Delivery, State0),
                   AppliedSD1 = mark_as_applied(Tick, [{SourceChunkId, DataId}], AppliedSD0),
                   ExpectedSD1 = sets:del_element({SourceChunkId, DataId}, ExpectedSD0),
                   make_delivery_ack(C#command.reply_fun),
                   State1#state{expected_sd = ExpectedSD1, applied_sd = AppliedSD1};
                 false ->
                   make_delivery_err(C#command.reply_fun, need_sync)
               end;
             false ->
               case was_applied({SourceChunkId, DataId}, DTick, State0#state.applied_sd) of
                 true ->
                   make_delivery_ack(C#command.reply_fun);
                 false ->
                   lager:error([{chkey, ChKey}], "Links are not consistent! unexpected delivery in ~p of ~p ", [Tick, C])
               end,
               State0
           end,
  {next_state, ready, State2};

%% handle delivery for future tick
ready(Command = #command{comm_data = #delivery{tick = DTick}},
    State0 = #state{tick = Tick, received_future_sd = FutureSD0}) when DTick > Tick ->
  lager:debug([{chkey, State0#state.chkey}], "in ~p received future delivery for ~p ~p", [Tick, DTick, Command]),
  FutureSD1 = add_to_future(Command, FutureSD0),
  {next_state, ready, State0#state{received_future_sd = FutureSD1}};

%% handle delivery for... past tick.
ready(Command = #command{comm_data = #delivery{tick = DTick, source_chunk_id = SourceChunkId, data_id = DataId}, reply_fun = ReplyFun},
    State = #state{tick = Tick, chkey = ChKey, applied_sd = AppliedSD}) when DTick < Tick ->
  case was_applied({SourceChunkId, DataId}, DTick, AppliedSD) of
    true ->
      make_delivery_ack(ReplyFun);
    false ->
      lager:error([{chkey, ChKey}], "~p Received past delivery in ~p of ~p ", [ChKey, Tick, Command])
  end,
  {next_state, ready, State};

%% handle clients events
ready(#command{comm_data = #client_event{name = subscribe, client = Client}, reply_fun = ReplyFun},
    State0 = #state{chunk_mod = Mod, tick = T, chunk_state = ChunkState0, subscriptions = Subscriptions0}) ->
  State1 = case maps:is_key(Client, Subscriptions0) of
             true ->
               ReplyFun({error, already_subscribed}),
               State0;
             false ->
               case Mod:client_subscribe(Client, ChunkState0) of
                 {allow, SyncData, ChunkState1} ->
                   ReplyFun({ok, T, SyncData}),
                   State0#state{
                     subscriptions = maps:put(
                       Client,
                       #subscription{
                         ts = T,
                         tref = gen_fsm:send_event_after(?CLIENT_HEART_TIMEOUT, {timeout, {heart, Client}})},
                       Subscriptions0),
                     chunk_state = ChunkState1};
                 {disallow, Reason, ChunkState1} ->
                   ReplyFun({error, Reason}),
                   State0#state{chunk_state = ChunkState1}
               end
           end,
  {next_state, ready, State1};

ready(#command{comm_data = #client_event{name = unsubscribe, client = Client}, reply_fun = ReplyFun},
    State0 = #state{chunk_mod = Mod, chunk_state = ChunkState0, subscriptions = Subscriptions0}) ->
  State1 = case maps:is_key(Client, Subscriptions0) of
             false ->
               ReplyFun({error, not_subscribed}),
               State0;
             true ->
               case Mod:client_unsubscribe(Client, ChunkState0) of
                 {ok, ChunkState1} ->
                   ReplyFun(ok),
                   State0#state{
                     subscriptions = maps:remove(Client, Subscriptions0),
                     chunk_state = ChunkState1}
               end
           end,
  {next_state, ready, State1};

ready(#command{comm_data = #client_event{name = event, client = Client, data = EventData}, reply_fun = ReplyFun},
    State0 = #state{chunk_mod = Mod, chunk_state = ChunkState0, subscriptions = Subscriptions0}) ->
  State1 = case maps:is_key(Client, Subscriptions0) of
             true -> %% TODO not subscribed should be allowed too?
               case Mod:handle_event(Client, EventData, ChunkState0) of
                 {reply, Reply, ChunkState1} ->
                   ReplyFun(Reply),
                   State0#state{chunk_state = ChunkState1};
                 {noreply, ChunkState1} ->
                   State0#state{chunk_state = ChunkState1}
               end;
             false ->
               ReplyFun({error, not_subscribed}),
               State0
           end,
  {next_state, ready, State1};

ready(#command{comm_data = #client_event{name = get_request, client = Client, data = RequestData}, reply_fun = ReplyFun},
    State0 = #state{chunk_mod = Mod, chunk_state = ChunkState0}) ->
  State1 = case Mod:handle_get_request(Client, RequestData, ChunkState0) of
             {ok, Response, ChunkState1} ->
               ReplyFun(Response),
               State0#state{chunk_state = ChunkState1}
           end,
  {next_state, ready, State1};

ready(#command{comm_data = #client_event{name = heart, client = Client}, reply_fun = ReplyFun},
    State0 = #state{subscriptions = Subscriptions0}) ->
  State1 = case maps:find(Client, Subscriptions0) of
             error ->
               ReplyFun({error, not_subscribed}),
               State0;
             {ok, Sub = #subscription{tref = OldTRef}} ->
               gen_fsm:cancel_timer(OldTRef),
               TRef = gen_fsm:send_event_after(?CLIENT_HEART_TIMEOUT, {timeout, {heart, Client}}),
               ReplyFun(heart),
               State0#state{subscriptions = maps:put(Client, Sub#subscription{tref = TRef}, Subscriptions0)}
           end,
  {next_state, ready, State1};

ready({timeout, {heart, Client}} = T, State0 = #state{subscriptions = Subscriptions0}) ->
  lager:warning([{chkey, State0#state.chkey}], "client removed ~p", [T]),
  {next_state, ready, State0#state{subscriptions = maps:remove(Client, Subscriptions0)}};

ready(Event, State = #state{chkey = ChKey}) ->
  lager:warning([{chkey, ChKey}], "unexpected event in ready ~p", [State#state.chkey, Event]),
  {next_state, ready, State}.

%% forward response
handoff(Response = #response{}, State) ->
  multiverse_chunk_vnode:forward_to_chunk(State#state.chkey, Response),
  {next_state, handoff, State};

%% forward command
handoff(Command = #command{}, State) ->
  multiverse_chunk_vnode:forward_to_chunk(State#state.chkey, Command),
  {next_state, handoff, State};

handoff(Event, State = #state{chkey = ChKey}) ->
  lager:warning([{chkey, ChKey}], "fsm handoff event ~p", [Event]),
  {next_state, handoff, State}.

handle_event({stop, Reason}, _StateName, State) ->
  {stop, Reason, State};

handle_event(handoff_cancelled, handoff, State = #state{chkey = ChKey}) ->
  lager:error([{chkey, ChKey}], "handoff_cancelled in handoff state ~p", [ChKey]),
  gen_fsm:send_event_after(0, try_tick),
  %ReqRequests = make_new_requests(ChKey, Tick, RequestsList),
  {next_state, ready, State#state{}};

handle_event(Command, handoff, State) ->
  lager:warning("~p fsm handoff command ~p", [State#state.chkey, Command]),
  {next_state, handoff, State};

handle_event(#command{comm_data = persist_chunk_state, reply_fun = ReplyFun}, StateName,
    State = #state{chunk_mod = Mod, chunk_state = ChunkState}) ->
  {ok, ChunkData} = Mod:persist_state(ChunkState),
  ReplyFun(ChunkData),
  {next_state, StateName, State#state{chunk_state = ChunkState}};

handle_event(Event, StateName, State) ->
  lager:warning("~p unexpected event in state ~p  ~p", [State#state.chkey, StateName, Event]),
  {next_state, StateName, State}.

handle_sync_event(in_handoff_state, _From, handoff, State) ->
  {reply, true, handoff, State};
handle_sync_event(in_handoff_state, _From, StateName, State) ->
  {reply, false, StateName, State};

%% raw debug command
handle_sync_event(get_chunk_state, _From, StateName, State = #state{chunk_mod = Mod, chunk_state = ChunkState0}) ->
  {ok, Response, ChunkState1} = Mod:extract_sync_data({manual, {0, 0}}, ChunkState0),
  {reply, Response, StateName, State#state{chunk_state = ChunkState1}};

handle_sync_event(handle_handoff, _From, _StateName, State = #state{chunk_mod = Mod, chunk_state = ChunkState}) ->
  {ok, ChunkData} = Mod:migrate_state(ChunkState),
  HandoffState = state_to_handoff_state(State),
  {reply, {ok, HandoffState#handoff_state{chunk_data = ChunkData}}, handoff, State};

handle_sync_event(_Event, _From, StateName, State) ->
  Reply = ok,
  {reply, Reply, StateName, State}.

handle_info(Info, StateName, State = #state{chunk_mod = Mod, chunk_state = ChunkState0}) ->
  {ok, ChunkState1} = Mod:handle_info(Info, ChunkState0),
  {next_state, StateName, State#state{chunk_state = ChunkState1}}.

terminate(Reason, _StateName, #state{chunk_mod = Mod, chunk_state = ChunkState}) ->
  Mod:terminate(Reason, ChunkState),
  ok.

code_change(_OldVsn, StateName, State, _Extra) ->
  {ok, StateName, State}.

%% Internal functions

make_command({World, DestChunkId}, CommandData) ->
  multiverse_chunk_vnode:make_chunk_command({World, DestChunkId}, CommandData),
  #command_stamp{dest_chkey = {World, DestChunkId}, comm_data = CommandData}.
make_command({World, DestChunkId}, CommandData, ReqId) ->
  multiverse_chunk_vnode:make_chunk_command({World, DestChunkId}, CommandData, ReqId),
  #command_stamp{dest_chkey = {World, DestChunkId}, comm_data = CommandData}.

pre_tick(State0 = #state{chunk_mod = Mod, chkey = {_World, ChunkId}, tick = Tick, chunk_state = ChunkState0}) ->
  {ok, {deliver, Deliveries}, {expect, Expectations}, ChunkState1} =
    Mod:sync_data_links(Tick, ChunkId, ChunkState0),
  State1 = State0#state{chunk_state = ChunkState1},
  State2 = make_acknowledgments(Expectations, State1),
  State3 = make_deliveries(Deliveries, State2#state{deliver_sd = #{}}),
  State3.


make_acknowledgments(Expectations, State0 = #state{tick = Tick, update_receive_fun = UpdFun,
  applied_sd = AppliedSD, received_future_sd = FutureSD}) ->
  {Matched, NotMatched, NewFutureSD} = match_expectations(Tick, Expectations, FutureSD),
  {ToApply, ToSync} = lists:partition(
    fun({_, #command{comm_data = #delivery{type = Type}}}) ->
      can_delivery_be_applied(Type, UpdFun)
    end,
    Matched),
  State1 = apply_received(ToApply, State0),
  [make_delivery_ack(ReplyFun) || {_, #command{reply_fun = ReplyFun}} <- ToApply],
  [make_delivery_err(ReplyFun, need_sync) || {_, #command{reply_fun = ReplyFun}} <- ToSync],
  NewAppliedSD = mark_as_applied(Tick, [Key || {Key, _} <- ToApply], AppliedSD),
  State1#state{
    expected_sd = sets:from_list(NotMatched),
    received_future_sd = NewFutureSD,
    applied_sd = NewAppliedSD}.

make_delivery_ack(ReplyFun) ->
  ReplyFun(#delivery_ack{}).

make_delivery_err(ReplyFun, Reason) ->
  ReplyFun(#delivery_err{reason = Reason}).

match_expectations(Tick, Expectations, FutureSD) ->
  case maps:find(Tick, FutureSD) of
    error ->
      {[], Expectations, FutureSD};
    {ok, CurrentSD} ->
      {Matched, NotMatched, LeftCurrent} = match_current(Expectations, [], [], CurrentSD),
      case maps:size(LeftCurrent) > 0 of
        true ->
          lager:error("Links are not consistent! unexpected delivery in ~p of ~p ", [Tick, io_lib:format("~p", [CurrentSD])]);
        false ->
          ok
      end,
      {Matched, NotMatched, maps:remove(Tick, FutureSD)}
  end.

add_to_future(Delivery = #command{comm_data = #delivery{tick = Tick, source_chunk_id = SourceChunkId, data_id = DataId}}, FutureSD) ->
  Key = {SourceChunkId, DataId},
  case maps:find(Tick, FutureSD) of
    error ->
      maps:put(Tick, maps:put(Key, Delivery, #{}), FutureSD);
    {ok, TickMap} ->
      maps:put(Tick, maps:put(Key, Delivery, TickMap), FutureSD)
  end.

match_current([], Matched, NotMatched, CurrentSD) ->
  {Matched, NotMatched, CurrentSD};
match_current([HExp | Tail], Matched, NotMatched, CurrentSD) ->
  case maps:find(HExp, CurrentSD) of
    {ok, Value} ->
      match_current(
        Tail,
        [{HExp, Value} | Matched],
        NotMatched,
        maps:remove(HExp, CurrentSD));
    error ->
      match_current(Tail, Matched, [HExp | NotMatched], CurrentSD)
  end.


can_delivery_be_applied(update, undefined) -> false;
can_delivery_be_applied(_Type, _UpdFun) -> true.

apply_received([], State) -> State;
apply_received([{_Exp, #command{comm_data = Delivery}} | Tail], State0) ->
  State1 = apply_delivery(Delivery, State0),
  apply_received(Tail, State1).


apply_delivery(#delivery{source_chunk_id = SourceChunkId, data_id = DataId, data = Data, type = sync},
    State = #state{chunk_mod = Mod, chunk_state = ChunkState0}) ->
  case Mod:receive_sync_data({SourceChunkId, DataId}, Data, ChunkState0) of
    {ok, ChunkState1} ->
      State#state{chunk_state = ChunkState1};
    {ok, UpdFun, ChunkState1} ->
      State#state{update_receive_fun = UpdFun, chunk_state = ChunkState1}
  end;

apply_delivery(#delivery{source_chunk_id = SourceChunkId, data_id = DataId, data = Data, type = update},
    State = #state{update_receive_fun = UpdFun0, chunk_state = ChunkState0}) when UpdFun0 /= undefined ->
  case UpdFun0({SourceChunkId, DataId}, Data, ChunkState0) of
    {ok, ChunkState1} ->
      State#state{chunk_state = ChunkState1};
    {ok, UpdFun1, ChunkState1} ->
      State#state{update_receive_fun = UpdFun1, chunk_state = ChunkState1}
  end.

%% helps me to debug consistency in synchronization
mark_as_applied(_Tick, [], AppliedSD) -> AppliedSD;
mark_as_applied(Tick, [Exp | Tail], AppliedSD) ->
  case maps:find(Exp, AppliedSD) of
    error ->
      mark_as_applied(Tick, Tail, maps:put(Exp, Tick, AppliedSD));
    {ok, LastTick} when LastTick =< Tick ->
      mark_as_applied(Tick, Tail, maps:put(Exp, Tick, AppliedSD));
    {ok, LastTick} ->
      lager:error("logical tick synchronization error: ~p < ~p ?", [LastTick, Tick]),
      mark_as_applied(Tick, Tail, maps:put(Exp, Tick, AppliedSD))
  end.

was_applied(Exp, Tick, AppliedSD) ->
  case maps:find(Exp, AppliedSD) of
    error -> false;
    {ok, LastTick} when LastTick =< Tick -> true;
    {ok, LastTick} ->
      lager:error("Is this even possible? ~p < ~p", [LastTick, Tick]),
      false
  end.

make_deliveries([], State) -> State;
make_deliveries([{DestChunkId, DataId} | Tail], State0) ->
  State1 = make_new_delivery({DestChunkId, DataId}, State0),
  make_deliveries(Tail, State1).

%% TODO remove duplicated code
make_new_delivery({DestChunkId, DataId}, State =
  #state{chunk_mod = Mod, chkey = {World, ChunkId}, tick = Tick, update_extract_fun = undefined, deliver_sd = Deliveries}) ->
  ReqId = mk_reqid(),
  case Mod:extract_sync_data({DestChunkId, DataId}, State#state.chunk_state) of
    {ok, UpdateFun, ExtractedData, ChunkState} ->
      Stamp = make_command(
        {World, DestChunkId},
        #delivery{tick = Tick, source_chunk_id = ChunkId, data_id = DataId, data = ExtractedData, type = sync},
        ReqId),
      State#state{
        update_extract_fun = UpdateFun,
        deliver_sd = maps:put(ReqId, Stamp, Deliveries),
        chunk_state = ChunkState};
    {ok, ExtractedData, ChunkState} ->
      Stamp = make_command(
        {World, DestChunkId},
        #delivery{tick = Tick, source_chunk_id = ChunkId, data = ExtractedData, data_id = DataId, type = sync}, ReqId),
      State#state{
        deliver_sd = maps:put(ReqId, Stamp, Deliveries),
        chunk_state = ChunkState}
  end;

make_new_delivery({DestChunkId, DataId}, State =
  #state{chkey = {World, ChunkId}, tick = Tick, update_extract_fun = UpdateFun, deliver_sd = Deliveries}) ->
  ReqId = mk_reqid(),
  case UpdateFun({DestChunkId, DataId}, State#state.chunk_state) of
    {ok, UpdateFun, ExtractedData, ChunkState} ->
      Stamp = make_command(
        {World, DestChunkId},
        #delivery{tick = Tick, source_chunk_id = ChunkId, data_id = DataId, data = ExtractedData, type = update},
        ReqId),
      State#state{
        update_extract_fun = UpdateFun,
        deliver_sd = maps:put(ReqId, Stamp, Deliveries),
        chunk_state = ChunkState};
    {ok, ExtractedData, ChunkState} ->
      Stamp = make_command(
        {World, DestChunkId},
        #delivery{tick = Tick, source_chunk_id = ChunkId, data_id = DataId, data = ExtractedData, type = update},
        ReqId),
      State#state{
        deliver_sd = maps:put(ReqId, Stamp, Deliveries),
        chunk_state = ChunkState}
  end.

retry_command(ReqId, #command_stamp{dest_chkey = DestChunkId, comm_data = CommandData}) ->
  make_command(DestChunkId, CommandData, ReqId).

was_sended(ReqId, Deliveries) ->
  maps:is_key(ReqId, Deliveries).

mark_as_delivered(ReqId, Deliveres) ->
  maps:remove(ReqId, Deliveres).

are_deliveres_fulfilled(Deliveries) ->
  maps:size(Deliveries) == 0.

are_expectations_fulfilled(Expectations) ->
  sets:size(Expectations) == 0.

handle_update_clients_result(UpdateResult,
    State0 = #state{chkey = {World, ChunkId}, update_method = multicast, tick = Tick, proxy_mod = ProxyMod, subscriptions = Subscriptions}) ->
  case UpdateResult of
    {ok, UpdateData, _} ->
      ProxyMod:multicast(World, ChunkId, Tick, maps:keys(Subscriptions), UpdateData),
      State0
  %% TODO implement more update options
  end.


mk_reqid() -> erlang:phash2(erlang:now()).


handoff_state_to_state(#handoff_state{
  tick = Tick,
  update_extract_fun = ExtractFun,
  update_receive_fun = ReceiveFun,
  deliver_sd = DeliverSD,
  expected_sd = ExpectedSD,
  received_future_sd = ReceivedSD,
  applied_sd = AppliedReceivedSD,
  subscriptions = Subscriptions}, State) ->
  State#state{
    tick = Tick,
    update_extract_fun = ExtractFun,
    update_receive_fun = ReceiveFun,
    deliver_sd = DeliverSD,
    expected_sd = ExpectedSD,
    received_future_sd = ReceivedSD,
    applied_sd = AppliedReceivedSD,
    subscriptions = Subscriptions}.

state_to_handoff_state(#state{
  tick = Tick,
  update_extract_fun = ExtractFun,
  update_receive_fun = ReceiveFun,
  deliver_sd = DeliverSD,
  expected_sd = ExpectedSD,
  received_future_sd = ReceivedSD,
  applied_sd = AppliedReceivedSD,
  subscriptions = Subscriptions}) ->
  #handoff_state{
    tick = Tick,
    update_extract_fun = ExtractFun,
    update_receive_fun = ReceiveFun,
    deliver_sd = DeliverSD,
    expected_sd = ExpectedSD,
    received_future_sd = ReceivedSD,
    applied_sd = AppliedReceivedSD,
    subscriptions = Subscriptions}.

