-define(APP, multiverse).


%% TODO to fix. types are useless now. parameterize it?

-type chkey() :: {world_name(), chunk_id()}.
-type world_name() :: atom().
-type chunk_id() :: any().

-type tick() :: non_neg_integer().

-type chunk_data() :: any().

-type world_config() :: proplists:proplist().
-type world_metadata() :: proplists:proplist().

-type update_method() :: multicast | multicast_groups | individual.

-type client() :: any().